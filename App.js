/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
} from 'react-native';

const App = () => {
  const screen = Dimensions.get('screen');
  const style = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#7ca1b4',
      alignItems: 'center',
      justifyContent: 'center',
    },
    card: {backgroundColor: '#fff', width: screen.width * 0.8},
    image: {height: screen.width * 0.8, width: screen.width * 0.8},
    nameText: {fontWeight: 'bold', color: '#20232A'},
    followtext: {fontWeight: 'bold', color: '#0095F6'},
    header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: 15,
      paddingVertical: 10,
    },
    footer: {
      paddingHorizontal: 15,
      paddingVertical: 10,
    },
  });
  return (
    <View style={style.container}>
      <View style={style.card}>
        <View style={style.header}>
          <Text style={style.nameText}>React Native School</Text>
          <Text style={style.followtext}>Follow</Text>
        </View>
        <Image
          style={style.image}
          resizeMode="cover"
          source={require('./assets/image.jpg')}
        />
        <View style={style.footer}>
          <Text>
            <Text style={style.nameText}>React Native School</Text>
            This has been a tutorial on how to build a layout with flexbox. I
            hope you enjoyed it!
          </Text>
        </View>
      </View>
    </View>
  );
};

export default App;
